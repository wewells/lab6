#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));

  /*Create a deck of cards of size 52 (hint this should be an array) and
   *initialize the deck*/
    Card Deck[52];
    for (int j = 0;j<4;j++) {
        for (int k = 1;k<14;k++) {
            // casting an int between 0 and 3 (inclusive) into a Suit yields
            // the Suit with the number corresponding to that int
            Deck[(13*j)+k].suit = static_cast<Suit>(j);
            Deck[(13*j)+k].value = k;
        }
    }

  /*After the deck is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/
   // myrandom is the random formula provided
    random_shuffle(Deck,Deck+52,myrandom);

   /*Build a hand of 5 cards from the first five cards of the deck created
    *above*/
    Card Hand[5] = {Deck[0],Deck[1],Deck[2],Deck[3],Deck[4]};

    /*Sort the cards.  Links to how to call this function is in the specs
     *provided*/
     // suit_order provides a reference through which sort() can compare two
     // Card objects
    sort(&Hand[0],&Hand[4],suit_order);


    /*Now print the hand below. You will use the functions get_card_name and
     *get_suit_code */
     for (int j = 0;j<5;j++) {
         cout << setw(10) << right << get_card_name(Deck[j]) << " of "
            << get_suit_code(Deck[j]) << endl;
     }



  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool suit_order(const Card& lhs, const Card& rhs) {
  // IMPLEMENT
  // casting lhs.suit and rhs.suit into ints returns the integer corresponding
  // to the suit, so we can multiply that value by 15 and then add the value
  // of the individual card in question to get the "value" of each card.
  return (((static_cast<int>(lhs.suit)*13)+lhs.value) < 
    ((static_cast<int>(rhs.suit)*13)+rhs.value));
}

string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

string get_card_name(Card& c) {
  // IMPLEMENT
  switch (c.value) {
     case 1 :         return "Ace";
     case 11 :        return "Jack";
     case 12 :        return "Queen";
     case 13 :        return "King";
     // default catches all regular numerical cases and prints them as strings
     default:  {       
        stringstream ss;
        ss << c.value;
        return ss.str(); 
        }
   }
}
